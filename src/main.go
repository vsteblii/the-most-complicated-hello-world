package main

import (
	"crypto/ecdsa"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v5"
)

var privateKey *ecdsa.PrivateKey
var publicKey *ecdsa.PublicKey

var db = make(map[string]string)

func panicIfError(err error) {
	if err != nil {
		panic(err)
	}
}

func readAndSavePrivateRSAKeyFromFile(filePath string) {
	data, err := os.ReadFile(filePath)
	panicIfError(err)
	fmt.Println(string(data))

	key, err := jwt.ParseECPrivateKeyFromPEM(data)
	panicIfError(err)

	privateKey = key
}

func readAndSavePublicRSAKeyFromFile(filePath string) {
	data, err := os.ReadFile(filePath)
	panicIfError(err)
	fmt.Println(string(data))

	key, err := jwt.ParseECPublicKeyFromPEM(data)
	panicIfError(err)

	publicKey = key
}

func generateJWT(userId int) (string, error) {
	expirationTime := time.Now().Add(10 * time.Minute)
	token := jwt.NewWithClaims(jwt.SigningMethodES256,
		jwt.MapClaims{
			"iss": "the-most-auth-server",
			"sub": userId,
			"exp": expirationTime.Unix(),
		})

	tokenString, err := token.SignedString(privateKey)

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func verifyJWT(token string) error {
	t, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		fmt.Println("Methor", t.Method)
		_, ok := t.Method.(*jwt.SigningMethodECDSA)
		if !ok {
			return nil, errors.New("Failed to parse a token")
		}
		return publicKey, nil
	})

	if err != nil {
		fmt.Println("err", t)
		exp, err := t.Claims.GetExpirationTime()
		if err == nil {
			fmt.Println("exp", exp)
		}

		return err
	}
	fmt.Println(t)
	return nil
}

func setupCommonRouter(r *gin.Engine) {
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
}

func setupV1Router(r *gin.Engine) {
	apiV1 := r.Group("/v1")

	apiV1.GET("/user/:userId", func(c *gin.Context) {
		userId := c.Param("userId")
		fmt.Println(userId)
		err := verifyJWT("eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTUzMTExMDQsImlzcyI6InRoZS1tb3N0LWF1dGgtc2VydmVyIiwic3ViIjoxfQ.U3eYvEKkSBhx8ApvH3onqnwGWHRuDPQCIe3r8GzdRMmkY2B_cbdIkRCVXn5HZMJYfTUa3kdHIabn8j0thXp2Mw")
		if err != nil {
			fmt.Println(err.Error())
		}

		c.JSON(http.StatusOK, gin.H{})
	})

	apiV1.POST("/login/jwt", func(c *gin.Context) {
		//todo store token in HTTPS only Cookies
		jwt, err := generateJWT(1)

		if err != nil {
			// todo Write error to logs
			errors := []string{err.Error()}
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "fail",
				"errors": errors,
			})
			return
		}

		errors := []string{}
		c.JSON(http.StatusOK, gin.H{
			"status": "success",
			"errors": errors,
			"response": gin.H{
				"access_token": jwt,
			},
		})

	})

	// r.GET("/user/:name", func(c *gin.Context) {
	// 	user := c.Params.ByName("name")
	// 	value, ok := db[user]
	// 	if ok {
	// 		c.JSON(http.StatusOK, gin.H{"user": user, "value": value})
	// 	} else {
	// 		c.JSON(http.StatusOK, gin.H{"user": user, "status": "no value"})
	// 	}
	// })

	// Authorized group (uses gin.BasicAuth() middleware)
	// Same than:
	// authorized := r.Group("/")
	// authorized.Use(gin.BasicAuth(gin.Credentials{
	// 	  "foo":  "bar",
	// 	  "manu": "123",
	// }))
	// authorized := r.Group("/", gin.BasicAuth(gin.Accounts{
	// 	"foo":  "bar", // user:foo password:bar
	// 	"manu": "123", // user:manu password:123
	// }))

	// /* example curl for /admin with basicauth header
	//    Zm9vOmJhcg== is base64("foo:bar")

	// 	curl -X POST \
	//   	http://localhost:8080/admin \
	//   	-H 'authorization: Basic Zm9vOmJhcg==' \
	//   	-H 'content-type: application/json' \
	//   	-d '{"value":"bar"}'
	// */
	// authorized.POST("admin", func(c *gin.Context) {
	// 	user := c.MustGet(gin.AuthUserKey).(string)

	// 	// Parse JSON
	// 	var json struct {
	// 		Value string `json:"value" binding:"required"`
	// 	}

	// 	if c.Bind(&json) == nil {
	// 		db[user] = json.Value
	// 		c.JSON(http.StatusOK, gin.H{"status": "ok"})
	// 	}
	// })

	// return r
}

func main() {
	readAndSavePrivateRSAKeyFromFile("../keys/rsa_key.pem")
	readAndSavePublicRSAKeyFromFile("../keys/rsa_key_public.pem")
	ginEngine := gin.Default()
	setupCommonRouter(ginEngine)
	setupV1Router(ginEngine)
	ginEngine.Run(":8080")
}
